from modeltranslation.translator import translator, TranslationOptions
from .models import Post

class PostTranslationOptions(TranslationOptions):
    fields = ('title', 'text', 'place',)
    required_languages = ('es','en')

translator.register(Post, PostTranslationOptions)