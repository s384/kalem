from django.db import models
from django.utils.timezone import now
from django.contrib.auth.models import User
from ckeditor.fields import RichTextField

# Create your models here.

class Post(models.Model):
	title = models.CharField(max_length=100, verbose_name="Titulo")
	text = RichTextField(verbose_name="Texto")
	place = models.CharField(max_length=50, verbose_name="Lugar")
	image = models.ImageField(upload_to="blog", verbose_name="Imagen")
	author = models.ForeignKey(User, verbose_name="Autor", on_delete=models.CASCADE)
	published = models.DateTimeField(verbose_name="Fecha publicacion", default=now)
	created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha Creacion")
	updated = models.DateTimeField(auto_now=True, verbose_name="Fecha Actualizacion")

	class Meta:
		verbose_name = "Post"
		ordering = ["-updated"]

	def __str__(self):
		return self.title