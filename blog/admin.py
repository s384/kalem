from django.contrib import admin
from .models import Post
# Register your models here.

class PostAdmin(admin.ModelAdmin):
	readonly_fields = ('created', 'updated')
	list_display = ('title', 'updated')
	exclude = ('title', 'text', 'place',)

admin.site.register(Post, PostAdmin)