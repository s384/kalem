from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator
from .models import Post

# Create your views here.

def blog(request):
	posts_list = Post.objects.all()
	paginator = Paginator(posts_list, 6)

	page = request.GET.get('page')
	posts = paginator.get_page(page)
	return render(request, "blog/blog.html", {'posts':posts})

def detail(request, pk):
	post = get_object_or_404(Post, pk=pk)
	return render(request, 'blog/detail.html', {'post':post})