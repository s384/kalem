from .settings import *

# custom settings for local development
DEBUG = True

#Host
ALLOWED_HOSTS = ['*']

# Database in Sqlite
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

STATIC_URL = '/static/'
#STATIC_ROOT = '/home/turismokalempata/public_html/static'

# Media files
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, "media")
#MEDIA_ROOT = '/home/turismokalempata/public_html/media'


# emails
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
DEFAULT_FROM_EMAIL = 'no-responder@chagogroup.com'
