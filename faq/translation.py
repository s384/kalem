from modeltranslation.translator import translator, TranslationOptions
from .models import faq

class faqTranslationOptions(TranslationOptions):
    fields = ('title', 'answer',)
    required_languages = ('es','en')

translator.register(faq, faqTranslationOptions)