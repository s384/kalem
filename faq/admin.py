from django.contrib import admin
from .models import faq
# Register your models here.

class FaqAdmin(admin.ModelAdmin):
	readonly_fields = ('created', 'updated')
	list_display = ('title', 'order')
	exclude = ('title', 'answer')

admin.site.register(faq, FaqAdmin)