from django.db import models
from django.utils.timezone import now
from ckeditor.fields import RichTextField

# Create your models here.

class faq(models.Model):
	title = models.CharField(max_length=100, verbose_name="Pregunta")
	answer = RichTextField(verbose_name="Respuesta")
	order = models.SmallIntegerField(verbose_name="Orden", default=0)
	created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha Creacion")
	updated = models.DateTimeField(auto_now=True, verbose_name="Fecha Actualizacion")

	class Meta:
		verbose_name="preguntas frecuentes"
		verbose_name_plural="preguntas frecuentes"
		ordering = ["order","title"]

	def __str__(self):
		return self.title