# Para el contacto
from django.template.loader import render_to_string
from django.urls import reverse
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.template.loader import get_template
from django.core.mail import send_mail
from .models import faq
from .forms import ContactForm

# Create your views here.

class contact(TemplateView):
    template_name = 'faq/contact.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = ContactForm()
        
        faqs = faq.objects.all()
        args = {'context':context, 'faqs':faqs}
        return args

    def post(self, request, *args, **kwargs):
        template = get_template('faq/contact_template.txt')

        contact_name = request.POST.get('name')
        contact_email = request.POST.get('email')
        contact_title = request.POST.get('title')
        form_content = request.POST.get('message')

        context = {
            'contact_name': contact_name,
            'contact_email': contact_email,
            'contact_title': contact_title,
            'form_content': form_content,
        }
        content = template.render(context)


        try:
            email = send_mail(
                'Correo enviado desde la pagina web',
                content,
                contact_email,
                ['contacto@turismokalempatagonia.com','turismokalempatagonia@gmail.com'],
                fail_silently=False,
            )
            return redirect(reverse('contact')+'?ok')
        except:
            return redirect(reverse('contact')+'?fail')