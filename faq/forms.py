from django import forms

class ContactForm(forms.Form):
	name = forms.CharField(label="Nombre", required=True, widget=forms.TextInput(
			attrs={'class':'form-control', 'placeholder':'Escribe tu nombre'}
		))
	email = forms.EmailField(label="Email", required=True, widget=forms.EmailInput(
			attrs={'class':'form-control', 'placeholder':'Escribe tu email'}
		))
	title = forms.CharField(label="Asunto", required=True, widget=forms.TextInput(
			attrs={'class':'form-control', 'placeholder':'Escribe el motivo del mensaje'}
		))
	message = forms.CharField(label="Contenido", required=True, widget=forms.Textarea(
			attrs={'class':'form-control','rows':7, 'placeholder':'Escribe tu mensaje'}
		))