from django.db import models
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFit
from ckeditor.fields import RichTextField
from core.watermark import TextWatermark

# Create your models here.

class Services(models.Model):
	name = models.CharField(max_length=100, verbose_name="Nombre Servicio")
	description = RichTextField(verbose_name="Descripción")
	image = ProcessedImageField(upload_to='services', processors=[ResizeToFit(1280), TextWatermark()], format='JPEG', options={'quality': 70}, null=True, blank=True)
	order = models.SmallIntegerField(verbose_name="Orden", default=0)
	created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha Creacion")
	updated = models.DateTimeField(auto_now=True, verbose_name="Fecha Actualizacion")

	class Meta:
		verbose_name="Servicios"
		verbose_name_plural="Servicios"
		ordering = ["order","name"]

	def __str__(self):
		return self.name

class Products(models.Model):
	service = models.ForeignKey(Services, on_delete=models.CASCADE)
	name = models.CharField(max_length=100, verbose_name="Nombre Producto")
	description = RichTextField(verbose_name="Descripción")
	image = ProcessedImageField(upload_to='products', processors=[ResizeToFit(1280), TextWatermark()], format='JPEG', options={'quality': 70}, null=True, blank=True)
	file = models.FileField(upload_to="products", null=True, blank=True, max_length=100, verbose_name="Itinerario")
	url = models.URLField(max_length=200, null=True, blank=True, verbose_name="Enlace referencia")
	order = models.SmallIntegerField(verbose_name="Orden", default=0)
	state = models.BooleanField(default=True, verbose_name="Estado del producto (Activo)")
	created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha Creacion")
	updated = models.DateTimeField(auto_now=True, verbose_name="Fecha Actualizacion")

	class Meta:
		verbose_name="Producto"
		verbose_name_plural="Productos"
		ordering = ["service","order","name"]

	def __str__(self):
		return self.name