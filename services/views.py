from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from .models import Services, Products

# Create your views here.

def services(request, pk):
    service = get_object_or_404(Services, pk=pk)
    products = Products.objects.filter(service_id=pk).filter(state=True)
    return render(request, 'services/services.html', {'service': service,'products':products,})
