from django.contrib import admin
from .models import Services, Products
# Register your models here.

class ServicesAdmin(admin.ModelAdmin):
	readonly_fields = ('created', 'updated')
	list_display = ('name', 'order')
	exclude = ('name', 'description',)

class ProductsAdmin(admin.ModelAdmin):
	readonly_fields = ('created', 'updated')
	list_display = ('name', 'order','service')
	exclude = ('name', 'description',)

admin.site.register(Services, ServicesAdmin)
admin.site.register(Products, ProductsAdmin)