from modeltranslation.translator import translator, TranslationOptions
from .models import Services, Products

class ServicesTranslationOptions(TranslationOptions):
    fields = ('name', 'description',)
    required_languages = ('es','en')

class ProductsTranslationOptions(TranslationOptions):
    fields = ('name', 'description',)
    required_languages = ('es','en')

translator.register(Products, ProductsTranslationOptions)
translator.register(Services, ServicesTranslationOptions)