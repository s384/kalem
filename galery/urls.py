from django.urls import path
from . import views

urlpatterns = [
	path('', views.galery_list, name='galery'),
	path('<int:pk>/<slug:slug>', views.galery_detail, name='detail'),
]