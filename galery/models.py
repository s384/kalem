import uuid
from django.db import models
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFit
from django.utils.timezone import now
from core.watermark import TextWatermark

# Create your models here.

class Album(models.Model):
    title = models.CharField(max_length=100, verbose_name="Nombre Album")
    description = models.TextField(verbose_name="Descripcion")
    place = models.CharField(max_length=100, verbose_name="Lugar")
    thumb = ProcessedImageField(upload_to='galery', null=True,processors=[ResizeToFit(300)], format='JPEG', options={'quality': 90}, verbose_name="Imagen Principal")
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha Creacion")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha Actualizacion")
    slug = models.SlugField(max_length=50, unique=True)

    class Meta:
        verbose_name = "Album"
        verbose_name_plural = "Albumes"
        ordering = ["-created"]

    def __str__(self):
        return self.title

class AlbumImage(models.Model):
    image = ProcessedImageField(upload_to='galery', processors=[ResizeToFit(1280), TextWatermark(),], format='JPEG', options={'quality': 70})
    album = models.ForeignKey('album', on_delete=models.PROTECT)
    alt = models.CharField(max_length=255, default=uuid.uuid4)
    created = models.DateTimeField(auto_now_add=True)
    width = models.IntegerField(default=0)
    height = models.IntegerField(default=0)
    slug = models.SlugField(max_length=70, default=uuid.uuid4, editable=False)

    def __str__(self):
        return self.image.name

    class Meta:
        verbose_name = "Imagen - Album"
        verbose_name_plural = "Imagenes de los albumes"