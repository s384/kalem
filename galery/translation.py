from modeltranslation.translator import translator, TranslationOptions
from .models import Album

class AlbumTranslationOptions(TranslationOptions):
    fields = ('title', 'description', 'place',)
    required_languages = ('es','en')

translator.register(Album, AlbumTranslationOptions)