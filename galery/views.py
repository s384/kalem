from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator
from .models import Album, AlbumImage


# Create your views here.

def galery_list(request):
	albums_list = Album.objects.all()
	paginator = Paginator(albums_list, 6)

	page = request.GET.get('page')
	albums = paginator.get_page(page)
	return render(request, 'galery/galery_list.html', {'albums':albums})

def galery_detail(request, pk, slug):
	album = get_object_or_404(Album, pk=pk)
	images = AlbumImage.objects.filter(album_id=pk)
	return render(request, 'galery/galery_detail.html',{'album':album, 'images':images})