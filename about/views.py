from django.shortcuts import render
from .models import About, Employed

# Create your views here.

def about(request):
	abouts = About.objects.all()
	employeds = Employed.objects.all()
	return render(request, "about/about.html", {'abouts':abouts,'employeds':employeds})