from django.contrib import admin
from .models import About, Employed
# Register your models here.

class AboutAdmin(admin.ModelAdmin):
	exclude = ('title', 'about', 'team',)

class EmployedAdmin(admin.ModelAdmin):
	readonly_fields = ('created', 'updated')
	list_display = ('name', 'order')
	exclude = ('job',)

admin.site.register(Employed, EmployedAdmin)
admin.site.register(About, AboutAdmin)