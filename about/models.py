from django.db import models
from django.utils.timezone import now
from ckeditor.fields import RichTextField

from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFit
from core.watermark import TextWatermark

# Create your models here.

class About(models.Model):
	title = models.CharField(max_length=100, verbose_name="Empresa")
	about = RichTextField(verbose_name="Acerca de nosotros")
	team = RichTextField(verbose_name="Nuestro equipo")
	image1 = ProcessedImageField(upload_to='about', processors=[ResizeToFit(500), TextWatermark(),], format='JPEG', options={'quality': 70},
			  blank=True, null=True, verbose_name="Imagen 1")
	image2 = ProcessedImageField(upload_to='about', processors=[ResizeToFit(500), TextWatermark(),], format='JPEG', options={'quality': 70},
			  blank=True, null=True, verbose_name="Imagen 2")
	image3 = ProcessedImageField(upload_to='about', processors=[ResizeToFit(500), TextWatermark(),], format='JPEG', options={'quality': 70},
			  blank=True, null=True, verbose_name="Imagen 3")
	image4 = ProcessedImageField(upload_to='about', processors=[ResizeToFit(500), TextWatermark(),], format='JPEG', options={'quality': 70},
			  blank=True, null=True, verbose_name="Imagen 4")
	image5 = ProcessedImageField(upload_to='about', processors=[ResizeToFit(500), TextWatermark(),], format='JPEG', options={'quality': 70},
			  blank=True, null=True, verbose_name="Imagen 5")

	class Meta:
		verbose_name="empresa"

	def __str__(self):
		return self.title

class Employed(models.Model):
	name = models.CharField(max_length=50, verbose_name="Nombre")
	job = models.CharField(max_length=30, verbose_name="Cargo")
	image = models.ImageField(upload_to="about", verbose_name="Imagen")
	order = models.SmallIntegerField(verbose_name="Orden", default=0)
	facebook = models.URLField(max_length=200, blank=True, null=True, verbose_name="Facebook")
	twitter = models.URLField(max_length=200, blank=True, null=True, verbose_name="Twitter")
	instagram = models.URLField(max_length=200, blank=True, null=True, verbose_name="Instagram")
	created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha Creacion")
	updated = models.DateTimeField(auto_now=True, verbose_name="Fecha Actualizacion")

	class Meta:
		verbose_name="empleado"
		verbose_name_plural="empleados"
		ordering = ["order"]

	def __str__(self):
		return self.name