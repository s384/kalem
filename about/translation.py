from modeltranslation.translator import translator, TranslationOptions
from .models import About, Employed

class AboutTranslationOptions(TranslationOptions):
    fields = ('title', 'about', 'team',)
    required_languages = ('es','en')

class EmployedTranslationOptions(TranslationOptions):
    fields = ('job',)
    required_languages = ('es','en')

translator.register(About, AboutTranslationOptions)
translator.register(Employed, EmployedTranslationOptions)