from django.shortcuts import render
from services.models import Services, Products
from blog.models import Post
from .models import AlbumImage

def home(request):
	imagenes = AlbumImage.objects.all()
	services = Services.objects.all()
	return render(request, "core/home.html", {'imagenes':imagenes, 'services':services})

def sitemap(request):
	return render(request, "core/sitemap.xml")