import uuid
from django.db import models
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFit
from django.utils.timezone import now
from .watermark import TextWatermark
# Create your models here.

class AlbumImage(models.Model):
    title = models.CharField(default='Turismo Kalem Patagonia', max_length=25)
    image = ProcessedImageField(upload_to='core', processors=[ResizeToFit(1280), TextWatermark()], format='JPEG', options={'quality': 70}, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    width = models.IntegerField(default=0)
    height = models.IntegerField(default=0)
    slug = models.SlugField(max_length=70, default=uuid.uuid4, editable=False)

    def __str__(self):
        return self.title
       
    class Meta:
        verbose_name="Imagenes Portada"
        verbose_name_plural ="Imagenes Portada"