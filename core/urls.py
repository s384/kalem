from django.urls import path, include
from django.conf.urls import url
from . import views

urlpatterns = [
	path('', views.home, name='home'),
	url(r'^i18n/', include('django.conf.urls.i18n')),
	path('sitemap/', views.sitemap, name='sitemap'),
]