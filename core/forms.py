from django import forms
from .models import AlbumImage

class AlbumImageForm(forms.ModelForm):
    class Meta:
        model = AlbumImage
        exclude = []

    zip = forms.FileField(required=False)