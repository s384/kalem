import os
import uuid
import zipfile
import mysite.settings
from datetime import datetime
from zipfile import ZipFile

from django.contrib import admin
from django.core.files.base import ContentFile

from PIL import Image

from .models import AlbumImage
from .forms import AlbumImageForm

@admin.register(AlbumImage)
class AlbumImageModelAdmin(admin.ModelAdmin):
    form = AlbumImageForm
    exclude = ['width','height']

    def save_model(self, request, obj, form, change):
        if form.is_valid():
            if form.cleaned_data['zip'] != None:
                zip = zipfile.ZipFile(form.cleaned_data['zip'])
                for filename in sorted(zip.namelist()):

                    file_name = os.path.basename(filename)
                    if not file_name:
                        continue

                    data = zip.read(filename)
                    contentfile = ContentFile(data)

                    img = AlbumImage()
                    name = file_name.split(".")
                    name_edit = name[0]
                    filename = '{0}{1}.jpg'.format(name_edit[0:10], str(uuid.uuid4())[-13:])
                    img.title = filename
                    img.image.save(filename, contentfile)
                
                    filepath = '{0}/core/{1}'.format(mysite.settings.MEDIA_ROOT, filename)
                    with Image.open(filepath) as i:
                        img.width, img.height = i.size

                    img.save()
                zip.close()
            else:
                album = form.save(commit=False)
                album.created = datetime.now()
                album.save()