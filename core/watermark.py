from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
from PIL import ImageMath

import os
from django.conf import settings

class TextWatermark(object):
    def __init__(self):
        self.text = "Turismo Kalem Patagonia"
        self.url_font = os.path.join(settings.BASE_DIR, "core/static/fonts/MISTRAL.TTF")
        self._default_font = ImageFont.truetype(self.url_font, 40)
        self._shadow_font = ImageFont.truetype(self.url_font, 40)

    def process(self, image):
        rgba_image = image.convert('RGBA')
        text_overlay = Image.new('RGBA', rgba_image.size, (255, 255, 255, 0))
        image_draw = ImageDraw.Draw(text_overlay)
        text_size_x, text_size_y = image_draw.textsize(self.text, font=self._default_font)
        x = (rgba_image.size[0] / 2) - (text_size_x / 2)
        y = (rgba_image.size[1] / 4) - (text_size_y / 2)
        image_draw.text((x-1,y), self.text, font=self._shadow_font, fill=(0, 0, 0, 255))
        image_draw.text((x+1,y), self.text, font=self._shadow_font, fill=(0, 0, 0, 255))
        image_draw.text((x,y-1), self.text, font=self._shadow_font, fill=(0, 0, 0, 255))
        image_draw.text((x,y+1), self.text, font=self._shadow_font, fill=(0, 0, 0, 255))

        image_draw.text((x,y), self.text, font=self._default_font, fill=(255, 255, 255, 128))
        image_with_text_overlay = Image.alpha_composite(rgba_image, text_overlay)

        return image_with_text_overlay
