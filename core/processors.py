from services.models import Services

def ctx_dict(request):
	ctx = {}
	links =  Services.objects.all()
	
	for link in links:
		ctx[link.id] = link.name
	return {'ctx':ctx}
